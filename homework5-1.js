const fs = require('fs');

let readJSONtoObjPromise = (file, encode) => {
    return new Promise((resolve, reject) => {
        fs.readFile(file, encode = 'utf8', (err, done) => {
            if (err) reject(err);
            else {
                resolve(JSON.parse(done));
            }
        })
    })
}

let countSomethingByKey = function (data, condition) {
    let counter = {}
    for (const index in data) {
        let eachObj = data[index];
        for (const key in eachObj) {
            if (key == condition) {
                if (counter[eachObj[key]] == null) {
                    Object.defineProperty(counter, eachObj[key], {
                        writable: true,
                        value: 0
                    });
                }
                counter[eachObj[key]]++;
            }
        }
    }
    return counter;
}

// let eyeCount = async function () {
//     return await readJSONtoObjPromise('homework1-4.json').then(data => {
//         return countSomethingByKey(data, 'eyeColor');
//     })
// }


let counter = function (arg) {
    return new Promise((resolve, reject) => {
        readJSONtoObjPromise(arg).then(data => {
            resolve(data)
        });
    })
}

let eyeCount = function (arg) {
    return new Promise((resolve, reject) => {
        readJSONtoObjPromise(arg).then(data => {
            console.log(data);
            resolve(data);
        })
    })
}

let genderCount = function (arg) {
    return new Promise((resolve, reject) => {
        readJSONtoObjPromise(arg).then(data => {
            countSomethingByKey(data, arg)
        })
    })
}

// let genderCount = async function () {
//     return await readJSONtoObjPromise('homework1-4.json').then(data => {
//         return countSomethingByKey(data, 'gender');
//     })
// }


// eyeCount()
// .then((done) => console.log(done))

// genderCount()
// .then((done) => console.log(done))

module.exports = { readJSONtoObjPromise, countSomethingByKey, eyeCount, genderCount, counter };
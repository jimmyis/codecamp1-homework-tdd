// Homework Day 5-1
// Simple (and Stupid) version

const fs = require('fs');
const sourceJSON = 'homework1-4.json';

let totalEyeColor = {
    brown: null,
    green: null,
    blue: null
}

let totalGender = {
    male: null,
    female: null
}

let friendCount = [];

fs.readFile(sourceJSON, 'utf8', (err, json) => {
    let sourceJSON = JSON.parse(json);

    for (const index in sourceJSON) {
        let eachPerson = sourceJSON[index];
        for (const key in eachPerson) {
            if (key == 'eyeColor') {
                let eyeColor = eachPerson[key];
                totalEyeColor[eyeColor]++
            }
            if (key == 'gender') {
                let gender = eachPerson[key];
                totalGender[gender]++
            }
            if (key == 'friends') {
                let friends = eachPerson[key];
                eachPerson.friendCount = friends.length;
            }
        }
        let eachPersonFriend = {
            _id: eachPerson._id,
            friendCount: eachPerson.friendCount,
        }
        friendCount.push(eachPersonFriend)
    }
    
    // Test 'friendCount' after loop
    // console.log(friendCount);

    // Write file 1
    fs.writeFile(__dirname + '/homework5-1_eyes.json', JSON.stringify(totalEyeColor), function (err) {
        if (err) console.log(err)
        else console.log("Writing file 1 done.")
    })
    // Write file 2
    fs.writeFile(__dirname + '/homework5-1_gender.json', JSON.stringify(totalGender), function (err) {
        if (err) console.log(err)
        else console.log("Writing file 2 done.")
    })
    // Write file 3

    fs.writeFile(__dirname + '/homework5-1_friends.json', JSON.stringify(friendCount), function (err) {
        if (err) console.log(err)
        else console.log("Writing file 3 done.")
    })
})
const assert = require('assert');
const fs = require('fs');
const my = require('./homework5-1.js');

const readFileWithCallback = function (file, cb) {
    fs.readFile(file, 'utf8', function(err, data) {
        if (err) 
            cb(err, null);
        else 
            cb(null, data);
    })
}

const readFilePromise = function (file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf8', function(err, data) {
            if (err) reject(err)
            else resolve(JSON.parse(data))
        })
    })
}

describe('Codecamp TDD Homework', function() {
    describe('Test read files with #readFile()', function() {
        let sourcefile = 'homework1-4.json';
        let files = ['homework5-1_eyes.json', 'homework5-1_gender.json', 'homework5-1_friends.json'];
        files.forEach(filename => {
            it(`should read ${filename} file.`, function(done) {
                readFileWithCallback(filename, function (err, data) {
                    if (err) 
                        done(err);
                    else {
                        assert.strictEqual(data.length > 0, true, `file '${filename}' should not empty`);
                        done();
                    }
                });     
            });
        });
    })

    describe('#objectKey()', function() {
        it('should have same object key stucture as homework5-1_eyes.json', async function() {
            const dataA = await my.eyeCount('homework1-4.json');
            const dataB = await readFilePromise('homework5-1_eyes.json');
            assert.deepEqual(dataA, dataB, `invalid compared data`);
        });
        it('should have same object key stucture as homework5-1_gender.json', async function() {
            const dataA = await my.genderCount('homework1-4.json');
            const dataB = await readFilePromise('homework5-1_gender.json');
            const dataC = { male: 11, female: 12 };
            assert.deepEqual(dataA, dataB, `invalid compared data`);
        });
        it('should have same object key stucture as homework5-1_gender.json', function() {
        });
    });

    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', async function() {
            const data = await readFilePromise('homework5-1_friends.json');
            assert.equal(data.length, 23, `array size invalid`);
        });

        it('should have sum of eyes as 23', async function() {
            const data = await my.counter('homework5-1_eyes.json');
            const sum = Object.values(data).reduce((sum,num) => sum + num )
            assert.strictEqual(sum, 23, `array size invalid`)
        });
        
        it('should have sum of gender as 23', async function() {
            const data = await my.counter('homework5-1_gender.json');
            const sum = Object.values(data).reduce((sum,num) => sum + num )
            assert.strictEqual(sum, 23, `array size invalid`)
        });
    });
})